'use strict';
module.exports = (sequelize, DataTypes) => {
  const Pesanan = sequelize.define('Pesanan', {
    id_pelanggan: DataTypes.STRING,
    tanggal_pesanan: DataTypes.DATE,
    total_harga: DataTypes.INTEGER
  }, {});
  Pesanan.associate = function(models) {
    // associations can be defined here
    Pesanan.belongsTo(models.Pelanggan, {
	    foreignKey: 'id_pelanggan',
	    as: 'pelanggans'
	});
  };
  return Pesanan;
};