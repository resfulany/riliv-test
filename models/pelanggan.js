'use strict';
module.exports = (sequelize, DataTypes) => {
  const Pelanggan = sequelize.define('Pelanggan', {
    nama: DataTypes.STRING,
    tanggal_daftar: DataTypes.DATE,
    umur: DataTypes.INTEGER,
    no_hp: DataTypes.STRING
  }, {});
  Pelanggan.associate = function(models) {
    // associations can be defined here
    Pelanggan.hasMany(models.Pesanan, {
      foreignKey: 'id_pelanggan',
      as: 'pesanans',
    });
  };
  return Pelanggan;
};