var express = require('express');
var router = express.Router();

const pesananController = require('../controllers').pesanan;
const pelangganController = require('../controllers').pelanggan;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Riliv Test - Dainty Resfulany S' });
});

/* Pesanan Router */
router.get('/api/pesanan', pesananController.list);
router.get('/api/pesanan/:id', pesananController.getById);
router.get('/api/pesanan/:id_pelanggan/listbypelanggan', pesananController.getByIdPelanggan);
router.post('/api/pesanan', pesananController.add);
router.post('/api/pesanan/:id', pesananController.update);
router.post('/api/pesanan/:id/delete', pesananController.delete);

/* Pelanggan Router */
router.get('/api/pelanggan', pelangganController.list);
router.get('/api/pelanggan/:id', pelangganController.getById);
router.post('/api/pelanggan', pelangganController.add);
router.post('/api/pelanggan/:id', pelangganController.update);
router.post('/api/pelanggan/:id/delete', pelangganController.delete);

module.exports = router;
