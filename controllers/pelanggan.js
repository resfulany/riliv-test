const Pesanan = require('../models').Pesanan;
const Pelanggan = require('../models').Pelanggan;

module.exports = {
  list(req, res) {
    return Pelanggan
      .findAll({
        include: [],
        order: [
          ['createdAt', 'DESC'],
        ],
      })
      .then((pelanggans) => res.status(200).send(pelanggans))
      .catch((error) => { res.status(400).send(error); });
  },

  getById(req, res) {
    return Pelanggan
      .findByPk(req.params.id)
      .then((pelanggan) => {
        if (!pelanggan) {
          return res.status(404).send({
            message: 'Pelanggan Tidak Ditemukan',
          });
        }
        return res.status(200).send(pelanggan);
      })
      .catch((error) => res.status(400).send(error));
  },

  add(req, res) {
    return Pelanggan
      .create({
        id: req.body.id,
        nama: req.body.nama,
        tanggal_daftar: req.body.tanggal_daftar,
        umur: req.body.umur,
        no_hp: req.body.no_hp,
      })
      .then((pelanggan) => res.status(201).send(pelanggan))
      .catch((error) => res.status(400).send(error));
  },

  update(req, res) {
    return Pelanggan
      .findByPk(req.params.id)      
      .then(pelanggan => {
        if (!pelanggan) {
          return res.status(404).send({
            message: 'Pelanggan Tidak Ditemukan',
          });
        }
        return pelanggan
          .update({
            nama: req.body.nama || pelanggan.nama,
            tanggal_daftar: req.body.tanggal_daftar || pelanggan.tanggal_daftar,
            umur: req.body.umur || pelanggan.umur,
            no_hp: req.body.no_hp || pelanggan.no_hp,
          })
          .then(() => res.status(200).send(pelanggan))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },

  delete(req, res) {
    return Pelanggan
      .findByPk(req.params.id)
      .then(pelanggan => {
        if (!pelanggan) {
          return res.status(400).send({
            message: 'Pelanggan Tidak Ditemukan',
          });
        }
        return pelanggan
          .destroy()
          .then(() => res.status(204).send())
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },
};