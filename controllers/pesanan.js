const Pesanan = require('../models').Pesanan;
const Pelanggan = require('../models').Pelanggan;

module.exports = {
  list(req, res) {
    return Pesanan
      .findAll({
        include: [],
        order: [
          ['createdAt', 'DESC'],
        ],
      })
      .then((pesanans) => res.status(200).send(pesanans))
      .catch((error) => { res.status(400).send(error); });
  },

  getById(req, res) {
    return Pesanan
      .findByPk(req.params.id)
      .then((pesanan) => {
        if (!pesanan) {
          return res.status(404).send({
            message: 'Pesanan Tidak Ditemukan',
          });
        }
        return res.status(200).send(pesanan);
      })
      .catch((error) => res.status(400).send(error));
  },

  getByIdPelanggan(req, res) {
    return Pesanan
      .findAll({
        id_pelanggan: req.body.id_pelanggan,
      })
      .then((pesanans) => res.status(200).send(pesanans))
      .catch((error) => { res.status(400).send(error); });
  },

  add(req, res) {
    return Pesanan
      .create({
        id: req.body.id,
        id_pelanggan: req.body.id_pelanggan,
        tanggal_pesanan: req.body.tanggal_pesanan,
        total_harga: req.body.total_harga,
      })
      .then((pesanan) => res.status(201).send(pesanan))
      .catch((error) => res.status(400).send(error));
  },

  addPesananWithPelanggan(req, res) {
    return Pesanan
      .create({
        id: req.body.id,
        id_pelanggan: req.body.id_pelanggan,
        tanggal_pesanan: req.body.tanggal_pesanan,
        total_harga: req.body.total_harga,
      }, {
        include: [{
          model: Pelanggan,
          as: 'pelanggans'
        }]
      })
      .then((pesanan) => res.status(201).send(pesanan))
      .catch((error) => res.status(400).send(error));
  },

  update(req, res) {
    return Pesanan
      .findByPk(req.params.id)      
      .then(pesanan => {
        if (!pesanan) {
          return res.status(404).send({
            message: 'Pesanan Tidak Ditemukan',
          });
        }
        return pesanan
          .update({
            id_pelanggan: req.body.tanggal_pesanan || pesanan.id_pelanggan,
            tanggal_pesanan: req.body.tanggal_pesanan || pesanan.tanggal_pesanan,
            total_harga: req.body.total_harga || pesanan.total_harga,
          })
          .then(() => res.status(200).send(pesanan))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },

  delete(req, res) {
    return Pesanan
      .findByPk(req.params.id)
      .then(pesanan => {
        if (!pesanan) {
          return res.status(400).send({
            message: 'Pesanan Tidak Ditemukan',
          });
        }
        return pesanan
          .destroy()
          .then(() => res.status(204).send())
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },
};